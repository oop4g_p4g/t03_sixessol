////////////////////////////////////////////////////////////////////////
// OOP Tutorial: Simple C++ OO program to simulate a simple Dice Game SOLUTION
////////////////////////////////////////////////////////////////////////

//--------include libraries
#include <ctime>	//for time used in random number routines
#include <iostream>	//for cin >> and cout <<
#include <string>	//for string routines

using namespace std;

//--------RandomNumberGenerator class
class RandomNumberGenerator {
	public:
		RandomNumberGenerator();
		~RandomNumberGenerator();
		int getRandomValue( int) const;
	private:
		void seed();
};
RandomNumberGenerator::RandomNumberGenerator()
{
	cout << "\n___RandomNumberGenerator default constructor called\n";
	seed();	//reset the random number generator from current system time
}
RandomNumberGenerator::~RandomNumberGenerator()
{
	cout << "\n___RandomNumberGenerator destructor called\n";
}
void RandomNumberGenerator::seed() {
	srand(static_cast<unsigned int>(time(0)));
}
int RandomNumberGenerator::getRandomValue( int max) const {
	return ( rand() % max) + 1; //produce a random number in range [1..max]
}
//--------end of RandomNumberGenerator class

//--------Dice class
class Dice {
	public:
		Dice();
		~Dice();
		int getFace() const;
		void roll();
	private:
		int face_;	//number on dice
};
Dice::Dice() : face_(0)
{
//	cout << "\n___Dice default constructor called\n";
}
Dice::~Dice()
{
//	cout << "\n___Dice destructor called\n";
}
int Dice::getFace() const {
	return face_;	//get value of dice face
}
void Dice::roll() {
	static RandomNumberGenerator srng;
	face_ = srng.getRandomValue( 6); 	//roll dice
}
//--------end of Dice class

//--------Score class
class Score {
	public:
		Score();
		Score( const Score& s);
		~Score();
		int getAmount() const;
		void updateAmount( int);
	private:
		int amount_;
};
Score::Score()
: amount_( 0) 
{
//	cout << "\n___Score default constructor called\n";
}
Score::Score( const Score& s) 
: amount_( s.amount_)
{
//	cout << "\n___Score copy constructor called\n";
}
Score::~Score() {
//	cout << "\n___Score destructor called\n";
}
int Score::getAmount() const {
	return amount_;
}
void Score::updateAmount( int value) {
//increment when value>0, decrement otherwise
	amount_ += value;
}
//--------end of Score class

//--------Player class
class Player {
	public:
		Player();
		~Player();
		string getName() const;
		int getScoreAmount() const;
		void readInName();
		int readInNumberOfGoes() const;
		void updateScore( int);
	private:
		string name_;
		Score score_;
};
Player::Player() 
: name_(), score_() 
{
//	cout << "\n___Player default constructor called\n";
}
Player::~Player() {
//	cout << "\n___Player destructor called\n";
}
int Player::getScoreAmount() const {
	return score_.getAmount();
}
string Player::getName() const {
	return name_;
}
void Player::updateScore( int value) {
	score_.updateAmount( value);
}
int Player::readInNumberOfGoes() const {
//ask the user for the number of dice throws
	int num;
	cout << "\nHow many go do you want? ";
	cin >> num;
	return( num);
}
void Player::readInName() {
//ask the user for his/her name
	cout << "\nEnter your name? ";
	cin >> name_;
}
//--------end of Player class

//--------Game class
class Game {
	public:
		explicit Game(Player* pplayer);
		~Game();
		Player getPlayer() const;
		void displayData() const;
		void run();
	private:
		Player* p_player_;
		Dice firstDice_, secondDice_;
		int numberOfGoes_;
		void rollDices();
};
Game::Game(Player* pplayer) 
: p_player_(pplayer), firstDice_(), secondDice_(), numberOfGoes_(0) 
{
	p_player_->readInName();
	numberOfGoes_ = p_player_->readInNumberOfGoes();
//	cout << "\n___Game explicit conversion constructor called";
}
Game::~Game() {
//	cout << "\n___Game destructor called";
}
Player Game::getPlayer() const {
	return *p_player_;	//returns the player
}

void Game::displayData() const {
	cout << "\nPlayer is: " << p_player_->getName();
	cout << "\nScore is: " << p_player_->getScoreAmount() << endl;
}
void Game::run() {
	for( int i(1); i <= numberOfGoes_; i++)
	{
		rollDices();
		const int face1( firstDice_.getFace()) ;
		const int face2( secondDice_.getFace()) ;
		cout << "\nIn try no: "<< i << " \tdice values are: "  << face1 << " & "  << face2;
		if ( face1 == face2)
			p_player_->updateScore( face1);
		cout << "\tThe current score is: "  << p_player_->getScoreAmount();	//check current score
	}
}
void Game::rollDices() {
		firstDice_.roll();
		secondDice_.roll();
}
//--------end of Game class

//---------------------------------------------------------------------------
//with two dices
int main() {
	{
		Player player;
		Game twoDiceGame(&player);
		cout << "\n________________________";
		cout << "\nGame starting...";
		twoDiceGame.displayData();
		cout << "\n________________________";
		twoDiceGame.run();
		cout << "\n________________________";
		cout << "\nGame ended...";
		twoDiceGame.displayData();
		cout << "\n________________________";
	}
	cout << endl << endl;
	system("pause");
	return 0;
}


